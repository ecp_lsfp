class Resource < ActiveRecord::Base
  attr_accessible :name
  has_many :representations, :dependent => false
  #
  # load configuration data from appcfg.yml
  def self.load_config_data
    if ActiveRecord::Base.connection.tables.include?(self.name.tableize)
      self.delete_all
      rn=[]
      APP_CONFIG["resources"].keys.each do |r|
        rn << APP_CONFIG["resources"][r]['names'][0]
      end
      rn.uniq.each_with_index do |r,i|
        o= self.new(:name => r)
        o.id= i+1
        o.save!
      end
    end
  end
end
