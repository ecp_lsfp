class TempFile < ActiveRecord::Base
  attr_accessible :fpath, :ftype

  belongs_to :notification
  after_destroy :remove_file

  TMP_DATA_DIR= Rails.root.join('tmp')
  FILE_TYPE= {
    "lsf_soap_data" => ".xml",
    "lecture_members" => ".json"
  }

  def save_data(text)
    fileext= FILE_TYPE[self.ftype]
    filepath= Pathname.new(TMP_DATA_DIR) + "#{self.ftype}_#{uniq_string}#{fileext}"
    self.fpath= filepath.to_s
    File.open(self.fpath,"w:UTF-8") do |f|
      f.puts text.force_encoding("UTF-8")
    end
    Rails.logger.debug "TempFile#save_data: save data file: #{self.fpath}"
    self
  end

  def remove_file
    if fpath and File.exist?(fpath)
      File.delete(fpath)
      Rails.logger.debug "TempFile#remove_file: removing file: #{fpath}"
    end
  rescue => ex
    Rails.logger.error "TempFile: Exception removing #{fpath}"
    Rails.logger.error "TempFile: Exception removing #{ex.inspect}"
    Rails.logger.debug $!.backtrace.collect { |b| " > #{b}" }.join("\n")
  end

private

  def uniq_string
    SecureRandom.urlsafe_base64(20)
  end
end
