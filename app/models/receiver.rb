class Receiver < ActiveRecord::Base
  attr_accessible :name, :mid
  has_many :receiver_representations, :dependent => false
  has_many :representations, :through => :receiver_representations, :dependent => false

  # load configuration data from appcfg.yml
  def self.load_config_data
    if ActiveRecord::Base.connection.tables.include?(self.name.tableize)
      self.delete_all
      APP_CONFIG["receivers"]["participants"].each_with_index do |rcv,i|
        self.new do |o|
          o.name= rcv[0]
          o.mid= rcv[1]
          o.id= i+1
          o.save!
        end
      end
    end
  end
end

