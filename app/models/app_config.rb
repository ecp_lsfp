class AppConfig < ActiveRecord::Base
  attr_accessible :delay_ecs_notifications

  # load configuration data from appcfg.yml
  def self.load_config_data
    if ActiveRecord::Base.connection.tables.include?(self.name.tableize)
      unless AppConfig.first
        create!
      end
      ac= AppConfig.first
      ac.delay_ecs_notifications= APP_CONFIG["delay_ecs_notifications"]
      ac.save!
    end
  end
end
