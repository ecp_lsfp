class Notification < ActiveRecord::Base
  attr_accessible :oid, :pid, :status_id, :representation_id, :update_instance
  belongs_to :status
  belongs_to :type
  has_many :representations, :dependent => :destroy
  has_many :temp_files, :dependent => :destroy

  scope :recovery_objects,
    joins(:status).
    where("statuses.name = ? or statuses.name = ? or statuses.name = ?", "new", "lsf_data_fetched", "processed").
    order("id ASC")
  scope :recovery_object,
    recovery_objects.limit(1).readonly(false)

  scope :recovery_objects_undelayed,
    recovery_objects.where("statuses.name != ?", "processed")
  scope :recovery_object_undelayed,
    recovery_objects_undelayed.limit(1).readonly(false)

  scope :recovery_objects_delayed,
    joins(:status).
    where("statuses.name = ?", "processed").
    order("id ASC")

  scope :sent_to_ecs,
    joins(:status).
    where("statuses.name = ?", "sent").
    order("id ASC")

  scope :scope_first_sent, lambda { |type,oid|
    joins(:type,:status).
    where("oid = ? and types.name = ? and update_instance = ? and statuses.name != ?", oid, type, false, "failure")}

  scope :by_type_and_oid, lambda { |type,oid|
    joins(:type).
    where("types.name = ? and oid = ?", type, oid).
    order("id ASC")}

  scope :by_status, lambda { |name|
    joins(:status).
    where("statuses.name = ?", name).
    order("id ASC")}


  def self.first_sent(type,oid)
    Notification.scope_first_sent(type,oid).first
  end

  # LSF Workaround: Group scenarious couldn't be determined at creation of
  # courses, because in LSF GUI you couldn't select it until the first
  # update. So if the creation of the course is still in status processed,
  # because of delayed ECS notifications, the subsequent update just
  # overwrites the previous created representation.
  # Even without this insufficiency of LSF this makes sense, because it
  # reduces unnecessary updates. This optimization also applies to updates only
  # too.
  def optimize_delay_queue
    prevn= previous
    if prevn and prevn.status.name== "processed"
      unless prevn.update_instance
        self.update_instance= false
        self.save!
      end
      prevn.destroy
      Rails.logger.debug "Notification#optimize_delay_queue: Notification queue optimized: Replaced Notification (type:#{prevn.type.name}, id:#{prevn.id}, update_instance:#{prevn.update_instance}) with Notification (type:#{self.type.name}, id:#{self.id}, update_instance:#{self.update_instance})."
    end
  end

  # selects previous notification by type and oid
  def previous
    nots_by_type= Notification.by_type_and_oid(self.type.name, self.oid)
    idx_self= nots_by_type.index(self)
    notification= nil
    if idx_self== 0
      notification= nil
    else
      notification= nots_by_type[idx_self - 1]
    end
    notification
  end
end
