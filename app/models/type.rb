class Type < ActiveRecord::Base
  attr_accessible :name
  has_many :notifications, :dependent => false

  # load configuration data from appcfg.yml
  def self.load_config_data
    if ActiveRecord::Base.connection.tables.include?(self.name.tableize)
      self.delete_all
      APP_CONFIG["notifications_types"].keys.each_with_index do |nt,i|
        o= self.new(:name => nt)
        o.id= i+1
        o.save!
      end
    end
  end
end
