class ReceiverRepresentation < ActiveRecord::Base
  attr_accessible :receiver_id, :representation_id
  belongs_to :receiver
  belongs_to :representation
end
