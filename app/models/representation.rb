class Representation < ActiveRecord::Base
  attr_accessible :filename, :rid, :notification_id, :status_id
  has_many :receiver_representations, :dependent => :destroy
  has_many :receivers, :through => :receiver_representations, :dependent => :destroy
  belongs_to :notification
  belongs_to :status
  belongs_to :resource
  after_destroy :send_delete_event
  after_destroy :remove_representation_file

private

  def remove_representation_file
    file_path1= File.join(APP_CONFIG['ecp_resource_representations_dir'], self.filename)
    file_path2= File.join(ResourceProcessor::RESOURCE_TMP_DIR, self.filename)
    [file_path1, file_path2].each do |fp|
      File.delete(fp) if File.exist?(fp)
    end
    Rails.logger.debug "Representation#remove_representation_file: removing file: #{self.filename}"
  rescue
    Rails.logger.error "Exception removing #{self.filename}"
    Rails.logger.error $!.backtrace.collect { |b| " > #{b}" }.join("\n")
  end

  def send_delete_event
    # Only send an delete event when the initial resource will be deleted. All
    # the updated resources have all the same ECS resource id hence shouldn't
    # trigger a delete event.
    return if self.notification.update_instance
    # rid is set only when the processed resource will be copied to the
    # ecp_resource_representations_dir
    return unless self.rid
    ecs= HttpEcs.instance
    set_receiver_memberships_headers(self, ecs)
    ecs.connection.headers.update HttpEcs::CONTENT_TYPE_JSON
    resourcename= APP_CONFIG['resources'][self.notification.type.name]['names'][0]
    Rails.logger.debug "Representation#send_delete_event: resourcename=#{resourcename}"
    Rails.logger.debug "Representation#send_delete_event: current representation model:#{self.inspect}"
    ecs.connection[resourcename+"/#{rid}"].delete do |response, request, result, &block|
      case response.code
      when 200
        Rails.logger.info "Representation#send_delete_event: deleted #{resourcename}/#{rid} on ECS."
      when 404
        Rails.logger.info "Representation#send_delete_event: deletion of #{resourcename}/#{rid} on ECS failed (HTTP code 404)."
      else
        Rails.logger.info "Representation#send_delete_event: deletion of #{resourcename}/#{rid} on ECS failed (HTTP code #{response.code})."
      end
    end
  end

  include LsfpHelper
    # set_receiver_memberships_headers(notification, ecs)
end
