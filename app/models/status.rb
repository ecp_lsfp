class Status < ActiveRecord::Base
  attr_accessible :name
  has_many :notifications, :dependent => false
  has_many :representations, :dependent => false

  def self.load_config_data
    if ActiveRecord::Base.connection.tables.include?(self.name.tableize)
      self.delete_all
      %w(new lsf_data_fetched processed failure deleted sent unknown).each_with_index do |stat,i|
        o= self.new(:name => stat)
        o.id= i+1
        o.save!
      end
    end
  end
end
