class ResourceProcessor

  require "lsfp_exceptions" 

  RESOURCE_TMP_DIR = "#{Rails.root}/tmp" 

  def initialize(notification)
    @notification= notification
    @secure_random= SecureRandom.urlsafe_base64(20)
    @xml_doc= nil
  end
    
  def process_data
    if (lsf_soap_data_files= @notification.temp_files.where(:ftype => 'lsf_soap_data')).blank?
      logtext= "ResourceProcessor#process_data: Notification (id=#{@notification.id}) without lsf soap data." 
      Rails.logger.error logtext
      raise LsfProxy::NotificationFailureException
    end
    Rails.logger.debug "ResourceProcessor#process_data: load lsf soap data files: #{lsf_soap_data_files.map{|f|f.fpath}.join(", ")}"
    lsf_soap_data_files.each do |lsf_soap_data_file|
      File.open(lsf_soap_data_file.fpath, "r") do |file|
        @xml_doc= Nokogiri::XML(file)
      end
      case @notification.type.name
        when "lecture"
          process_lecture
          process_lecture_members
        when "course_catalog"
          process_course_catalog
        when "enrollments"
          process_enrollments
      end
    end
    @notification.optimize_delay_queue
  end
    
private

  File.class_eval do
    # pretty print JSON text
    def jpp json_string
      json_object = JSON.parse(json_string)     
      self.puts JSON.pretty_generate(json_object)
    end
    # compact print JSON text
    def jcp json_string
      json_object = JSON.parse(json_string)     
      self.puts JSON.generate(json_object)
    end
    def jp(json_string, mode="compact")
      if mode=="pretty"
        jpp json_string
      else
        jcp json_string
      end
    end
  end

  # pretty print JSON text
  def jpp json_string
    json_object = JSON.parse(json_string)
    JSON.pretty_generate(json_object)
  end
  # compact print JSON text
  def jcp json_string
    json_object = JSON.parse(json_string)     
    JSON.generate(json_object)
  end
  def jp(json_string, mode="compact")
    if mode=="pretty"
      jpp json_string
    else
      jcp json_string
    end
  end

  def totxt nodeset
    if (val=nodeset.text).blank?
      nil
    else
      val
    end
  rescue NoMethodError
    nil
  end
    
  def toint nodeset
    if (val=nodeset.text).blank?
      nil
    else
      val.to_i
    end
  rescue NoMethodError
    nil
  end


  ##################
  #    lecture     #
  ##################

  def process_lecture
    tz= APP_CONFIG['soap_client']['lsf']['timezone']
    if totxt(@xml_doc.css('Lectures')).blank?
      raise LsfProxy::NotificationFailureException, "empty lectures"
    end
    lecture= @xml_doc.css('Lectures > Lecture').first

    json_txt = Jbuilder.encode do |json|
      json.ignore_nil!
      json.lectureID totxt(lecture.css('> LectureID'))
      json.title totxt(lecture.css('> Title'))
      unless (dat= lecture.css('> InvolvedInstitutions > Institution').first.css('Tree ParentSection').last).blank?
        json.organisation totxt(dat.css('Header > Name'))
      end

      unless totxt(lecture.css('> InvolvedInstitutions')).blank?
        json.organisationalUnits lecture.css('> InvolvedInstitutions > Institution') do |institution|
          json.id totxt(institution.css('> HeaderID'))
          json.title totxt(institution.css('> Name'))
        end
      end

      json.number totxt(lecture.css('> Number'))
      json.term totxt(lecture.css('> Term'))
      json.termID totxt(lecture.css('> TermID'))
      json.lectureType totxt(lecture.css('> Type'))
      json.hoursPerWeek toint(lecture.css('> HoursPerWeekInTerm'))
      json.groupScenario map_groupscenario(lecture.css('> LearningManagementSystemConfiguration > SubGroups').text)

      unless totxt(lecture.css('> DegreePrograms')).blank?
        json.degreeProgrammes lecture.css('> DegreePrograms > DegreeProgram') do |dp|
          json.id totxt(dp.css('> DegreeID'))
          json.title totxt(dp.css('> Name'))
          json.courseUnitYearOfStudy do
            json.from toint(dp.css('> StartTerm'))
            json.to toint(dp.css('> EndTerm'))
          end
        end
      end

      unless totxt(lecture.css('> CourseCatalog')).blank?
        json.allocations lecture.css('> CourseCatalog > Section') do |allocation|
          json.parentID totxt(allocation.css('> HeaderID'))
          json.order toint(allocation.css('> Sort'))
        end
      end

      json.comment1 totxt(lecture.css('> Note'))
      json.recommendedReading totxt(lecture.css('> Literature'))
      json.prerequisites totxt(lecture.css('> Preconditions'))
      json.lectureAssessmentType totxt(lecture.css('> Proof'))

      unless totxt(lecture.css('> TargetAudience')).blank?
        json.targetAudiences do
          json.array! lecture.css('> TargetAudience').collect {|e| e.text}
        end
      end

      unless totxt(lecture.css('Links')).blank?
        json.links lecture.css('Links > Link') do |link|
          json.title totxt(link.css('> Name'))
          json.href totxt(link.css('> URL'))
        end
      end

      # involved persons
      ipa=[]
      lecture.css('> InvolvedPersons > Person').each do |p|
        ipa << {"firstName" => p.css('> FirstNames').text, "lastName" => p.css('> LastName').text}
      end
      # initializing group array
      if (group_length= lecture.css('> Dates > Date > GroupID').map{|g| g.text}.uniq.sort.length) == 0
        parallel_groups= false
        groups_arr= [[]]
      else
        parallel_groups= true
        groups_arr= []
        (1..group_length).each {|i| groups_arr << []}
      end
      # sorting dates to their groups
      lecture.css('> Dates > Date').each do |date|
        gid= toint(date.css('> GroupID'))
        if gid
          groups_arr[gid-1] << date
        else
          groups_arr[0] << date
        end
      end

      # groups
      unless groups_arr.flatten.blank?
        json.groups groups_arr do |group|
          json.id totxt(group[0].css('> GroupID'))
          json.comment totxt(group[0].css('> Note'))
          # getting persons
          pa=[]
          group.each do |date|
            date.css('> Persons > Person').each do |p|
              pa << {"firstName" => totxt(p.css('> FirstNames')), "lastName" => totxt(p.css('> LastName'))}
            end
          end
          # lecturers: concatenation of date->Persons und InvolvedPersons
          json.lecturers do
            json.array! (ipa + pa).uniq
          end
          # maxParticipants
          if (num_of_participants= toint(lecture.css('> NumberOfParticipants')))
            json.maxParticipants num_of_participants
          end
          ## takesPlace
          #if totxt(group[0].css('> TakingPlace'))
          #  json.takesPlace (group[0].css('> TakingPlace').text=="true") ? true : false
          #end

          # datesAndVenues
          canceled_dates_arr= []
          json.datesAndVenues group do |date|
            json.cycle totxt(date.css('> Rhythm'))
            json.venue totxt(date.css('> Room > RoomOfficialName'))
            json.firstDate do
              if totxt(date.css('> StartDate')) and totxt(date.css('> StartTime'))
                json.startDatetime ActiveSupport::TimeZone[tz].parse("#{date.css('> StartDate').text} #{date.css('> StartTime').text}")
                json.endDatetime ActiveSupport::TimeZone[tz].parse("#{date.css('> StartDate').text} #{date.css('> EndTime').text}")
              end
            end
            json.lastDate do
              if totxt(date.css('> EndDate')) and totxt(date.css('> EndTime'))
                json.startDatetime ActiveSupport::TimeZone[tz].parse("#{date.css('> EndDate').text} #{date.css('> StartTime').text}")
                json.endDatetime ActiveSupport::TimeZone[tz].parse("#{date.css('> EndDate').text} #{date.css('> EndTime').text}")
              end
            end
            date.css('> CancelledDates > CancelledDate').each do |cd|
              canceled_dates_arr << cd
            end
          end
          # canceledDates
          tmp_arr= []
          canceled_dates_uniq_arr= []
          canceled_dates_arr.each do |cd|
            dt= ActiveSupport::TimeZone[tz].parse("#{cd.css('> Date').text}")
            unless tmp_arr.include?(dt)
              canceled_dates_uniq_arr << {
                :datetime => dt,
                :comment => totxt(cd.css('> Note')) }
              tmp_arr << dt
            end
          end
          json.canceledDates do
            json.array! canceled_dates_uniq_arr do |date|
              json.datetime date[:datetime]
              json.comment date[:comment]
            end
          end unless canceled_dates_uniq_arr.blank?
        end
      end
    end

    # rid of representation will be determined in ecs_resource_generator
    # while sending
    representation= @notification.representations.create!("filename" => "courses_#{@secure_random}.json") 
    Resource.find_by_name(APP_CONFIG['resources']['lecture']['names'][0]).representations << representation

    # Select and create receiver memberships targets
    lms_target_type= totxt(lecture.css('> LearningManagementSystemConfiguration > Activated'))
    APP_CONFIG["receivers"]["participants"].map do |p| 
      p[0]== lms_target_type ? p[1] : nil
    end.compact.each do |mid|
      representation.receivers.create!("name" => lms_target_type, "mid" => mid)
      Rails.logger.debug "ResourceProcessor#process_lecture: create receiver (#{lms_target_type}, #{mid}"
    end

    File.open(File.join(RESOURCE_TMP_DIR,"#{representation.filename}"), "w") do |f|
      if APP_CONFIG['resources']['lecture']['pretty_print']
        f.puts jpp(json_txt)
      else
        f.puts json_txt
      end
    end
  end

  def map_groupscenario lsf_scenario
    (sc=APP_CONFIG['groupscenario'][lsf_scenario]).nil? ? 0 : sc
  end

  ##########################
  #    lecture_members     #
  ##########################

  def process_lecture_members
    tz= APP_CONFIG['soap_client']['lsf']['timezone']
    lecture= @xml_doc.css('Lectures > Lecture').first
    lecture_id= nil

    json_txt = Jbuilder.encode do |json|
      json.ignore_nil!
      lecture_id= totxt(lecture.css('> LectureID'))
      json.lectureID lecture_id

      # collect lecture persons
      lecpers_arr=[]
      Rails.logger.debug "ResourceProcessor#process_lecture_members: persons: #{lecture.css('> Dates > Date > Persons > Person > PersonID').map {|e| e.text}.uniq.inspect}"
      lecture.css('> Dates > Date > Persons > Person > PersonID').map {|e| e.text}.uniq.each do |person_id|
        Rails.logger.debug "ResourceProcessor#process_lecture_members: person: #{person_id}"
        groups_arr= []
        lecture.css('> Dates > Date').each do |group|
          group.css('> Persons > Person > PersonID').each do |person_id_2|
            if person_id== person_id_2.text
              groups_arr << { "num" => toint(group.css('> GroupID')), "role" => 0 }
            end
          end
        end
        lecpers_arr << {
          "personID" => person_id,
          "role" => 0,
          "groups" => groups_arr.uniq
        }
      end
      lecpers_arr.uniq!

      # collect involved persons 
      invp_arr= []
      lecture.css('> InvolvedPersons > Person').each do |invp|
        invp_arr << {
          "personID" => totxt(invp.css('> PersonID')),
          "role" => 0 # lecturer
        }
      end
      invp_arr.uniq!
          

      # collect student admins
      studadm_arr=[]
      lecture.css('> LearningManagementSystemConfiguration > StudAdmins > StudAdmin').each do |studadm|
        studadm_arr << {
          "personID" => totxt(studadm.css('> StudentID')),
          "role" => 2 # assistant (aka "Hiwi")
        }
      end
      studadm_arr.uniq!

      json.members do
        json.array! invp_arr + studadm_arr + lecpers_arr
      end
    end
    
    enrollments_notification= Notification.create!(
      :oid => lecture_id,
      :pid => nil,
      :update_instance => @notification.update_instance)
    enrollments_notification.status= Status.find_by_name("new")
    enrollments_notification.type= Type.find_by_name("enrollments")
    enrollments_notification.save!

    tf= TempFile.new(:ftype => "lecture_members")
    if APP_CONFIG['resources']['enrollments']['pretty_print']
      tf.save_data(jpp(json_txt)).save!
    else
      tf.save_data(jcp(json_txt)).save!
    end
    enrollments_notification.temp_files << tf

    # Select receiver memberships targets
    tf= TempFile.new(:ftype => "receiver_memberships")
    lms_target_type= totxt(lecture.css('> LearningManagementSystemConfiguration > Activated'))
    receiver_memberships= APP_CONFIG["receivers"]["participants"].map do |p| 
      p[0]== lms_target_type ? p : nil
    end.compact
    tf.save_data(receiver_memberships.to_json).save!
    enrollments_notification.temp_files << tf
  end




  ##################
  # enrollments    #
  ##################

  def process_enrollments
    tz= APP_CONFIG['soap_client']['lsf']['timezone']
    if totxt(@xml_doc.css('Enrollments')).blank?
      raise LsfProxy::NotificationFailureException, "empty enrollments"
    end
    enrollments= @xml_doc.css('Enrollments > Lecture > Enrollment')
    lecture_id= totxt(@xml_doc.css('Enrollments > LectureID'))
    lecture_members= []

    if (lecture_members_data_files= @notification.temp_files.where(:ftype => 'lecture_members')).blank?
      logtext= "ResourceProcessor#process_enrollments: Notification (id=#{@notification.id}) without lecture members data." 
      Rails.logger.error logtext
      raise LsfProxy::NotificationFailureException
    end
    Rails.logger.debug "ResourceProcessor#process_enrollments: load lecture members data files: #{lecture_members_data_files.map{|f|f.fpath}.join(", ")}"
    lecture_members_data_files.each do |lecture_member_data_file|
      File.open(lecture_member_data_file.fpath, "r") do |f|
        lecture_members += JSON.parse(f.read)["members"]
      end
    end

    json_txt = Jbuilder.encode do |json|
      json.ignore_nil!
      lecture_id= totxt(@xml_doc.css('Enrollments > Lecture > LectureID'))
      json.lectureID lecture_id
      lecstud_arr=[]
      enrollments.css('> MatriculationNumber').map {|e| e.text}.uniq.each do |person_id|
        groups_arr= []
        enrollments.each do |enrollment|
          enrollment.css('> MatriculationNumber').each do |person_id_2|
            if person_id== person_id_2.text
              groups_arr << { "num" => toint(enrollment.css('> GroupID')), "role" => 1 }
            end
          end
        end
        lecstud_arr << {
          "personID" => person_id,
          "role" => 1,
          "groups" => groups_arr
        }
      end
      json.members do
        json.array! lecture_members + lecstud_arr
      end
    end


    # rid of representation will be determined in ecs_resource_generator
    # while sending
    representation= @notification.representations.create!("filename" => "course_members_#{@secure_random}.json") 
    Resource.find_by_name(APP_CONFIG['resources']['enrollments']['names'][0]).representations << representation

    # Create receiver memberships targets
    if (receiver_memberships_data_files= @notification.temp_files.where(:ftype => 'receiver_memberships')).blank?
      logtext= "ResourceProcessor#process_enrollments: Notification (id=#{@notification.id}) without receiver memberships data." 
      Rails.logger.error logtext
      raise LsfProxy::NotificationFailureException
    end
    Rails.logger.debug "ResourceProcessor#process_data: loaded receiver memberships ata files: #{receiver_memberships_data_files.map{|f|f.fpath}.join(", ")}"
    receiver_memberships= []
    receiver_memberships_data_files.each do |receiver_memberships_data_file|
      File.open(receiver_memberships_data_file.fpath, "r") do |file|
        receiver_memberships += JSON.parse file.read
      end
    end
    receiver_memberships.each do |rmemb|
      representation.receivers.create!("name" => rmemb[0], "mid" => rmemb[1])
    end

    File.open(File.join(RESOURCE_TMP_DIR,"#{representation.filename}"), "w") do |f|
      if APP_CONFIG['resources']['enrollments']['pretty_print']
        f.puts jpp(json_txt)
      else
        f.puts json_txt
      end
    end
  end



  ##################
  # course catalog #
  ##################

  def process_course_catalog
    if totxt(@xml_doc.css('CourseCatalog')).blank?
      raise LsfProxy::NotificationFailureException
    end
    # rid of representation will be determined in ecs_resource_generator
    # while sending
    representation= @notification.representations.create!("filename" => "directory_trees_#{@secure_random}.json") 
    Resource.find_by_name(APP_CONFIG['resources']['course_catalog']['names'][0]).representations << representation

    File.open(File.join(RESOURCE_TMP_DIR,"#{representation.filename}"), "w") do |f|
      f.puts("{")
      #
      # root node
      rootID= totxt(@xml_doc.css('CourseCatalog > Section > RootID')[0])
      title= totxt(@xml_doc.css('CourseCatalog > Section > Title')[0])
      term= totxt(@xml_doc.css('CourseCatalog > Section > Term')[0])
      f.puts("\"rootID\": \"#{rootID}\",") if rootID
      f.puts("\"directoryTreeTitle\": \"#{title}\",") if title
      f.puts("\"term\": \"#{term}\",") if term
      f.puts("\"nodes\": [")
      #
      # subsequent nodes
      node= {}
      doc= @xml_doc.css('SubSection Section')
      doc.each do |nnode|
        hash_node= Hash.from_xml(nnode.to_xml)
        parent_node= find_parent_node(nnode, "Section")
        hash_parent_node= Hash.from_xml(parent_node.to_xml)
        node["id"]= hash_node["Section"]["SectionID"]
        node["title"]= hash_node["Section"]["Title"]
        if parent_node.name == "Tree"
          node["parent"]= { "id" => rootID, "title" => title }
        else
          node["parent"]= { "id" => hash_parent_node["Section"]["SectionID"], "title" => hash_parent_node["Section"]["Title"] }
        end
        f.print JSON.pretty_generate(node)
        if doc.last === nnode
          f.puts
        else
          f.puts(",")
        end
      end
      f.puts("]")
      f.puts("}")
    end

    # Select and create receiver memberships targets
    APP_CONFIG["receivers"]["participants"].each do |p| 
      representation.receivers.create!("name" => p[0], "mid" => p[1])
    end
  end


  # Get parent node from nnode, then search previous siblings until we find a
  # node with node name node_name (e.g. "Section"). If can't find such a node,
  # get a parent node from the last previous sibling found and start previous
  # sibling search again.
  def find_parent_node(nnode, node_name)
    #debugger
    start_node= nnode
    parent_node= nil
    parent_node_found= false
    until parent_node_found
      nnode= nnode.parent
      if nnode.name == "Tree"
        parent_node_found= true
        parent_node= nnode
      end
      while not parent_node_found and nnode.previous_sibling
        nnode= nnode.previous_sibling
        if nnode.name == node_name
          #puts "*** start  node: #{Hash.from_xml(start_node.to_xml)["Section"]["SectionID"]}"
          #puts "*** parent node: #{Hash.from_xml(nnode.to_xml)["Section"]["SectionID"]}"
          parent_node_found= true
          parent_node= nnode
        end
      end
    end
    parent_node 
  end

end
