class QueueSoapClient

  require 'savon'

  def initialize
    Savon.configure do |config|
      config.log = false            # disable logging
      #config.log_level = :info      # changing the log level
      #config.logger = Rails.logger  # using the Rails logger
    end
    @client = Savon::Client.new do |wsdl, http|
      http.auth.ssl.verify_mode= APP_CONFIG['soap_client']['queue']['ssl_verify_mode']
      wsdl.endpoint= APP_CONFIG['soap_client']['queue']['endpoint']
      wsdl.namespace= APP_CONFIG['soap_client']['queue']['namespace']
    end
  end

  def fetch
    response= @client.request(:wsdl, "fetchNotification")
    response.body[:fetch_notification_response][:return]
  end

  def delete
    @client.request(:wsdl, "deleteNotification")
  end

end
