module LsfpHelper


  # Sets the "X-EcsReceiverMemberships" HTML header.
  def set_receiver_memberships_headers(representation, ecs)
    header= { "X-EcsReceiverMemberships" => representation.receivers.map{|r| r.mid}.join(',') }
    ecs.connection.headers.update header
    Rails.logger.info "create resource http headers: #{ecs.connection.headers}"
    header
  end

  def self.enable_ecs_notifications
    ac= AppConfig.first
    ac.delay_ecs_notifications= false
    ac.save!
  end

  def self.disable_ecs_notifications
    ac= AppConfig.first
    ac.delay_ecs_notifications= true
    ac.save!
  end

  def self.ecs_notifications_delayed?
    AppConfig.first.delay_ecs_notifications
  end

  def self.pid_file
    Rails.root.join("tmp/pids/lsfp.pid")
  end

  def self.pid_file?
    File.exists? pid_file
  end

  def self.number_of_representations
    Notification.sent_to_ecs.map{|n| Notification.first_sent(n.type.name,n.oid)}.uniq.length
  end

  def self.number_of_delayed_representations
    Notification.recovery_objects_delayed.length
  end

  def self.number_of_failed_notifications
    Notification.by_status("failure").length
  end
end
