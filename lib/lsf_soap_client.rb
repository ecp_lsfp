class LsfSoapClient

  require 'cgi'


  def initialize(notification)
    Savon.configure do |config|
      config.log = false             # disable logging
      #config.log_level = :info      # changing the log level
      #config.logger = Rails.logger  # using the Rails logger
    end
    @client = Savon::Client.new do |wsdl, http|
      http.auth.ssl.verify_mode=:none
      #wsdl.endpoint= config['network']['protocol']+'://'+
      #                config['network']['ip']+':'+
      #                config['network']['port'].to_s
      #wsdl.namespace= config['soap']['name']
      #wsdl.document = "lsf_proxy.wsdl"
      #http.headers["SOAPAction"]= '"urn:SimpleWS#hello_world"'
      #wsdl.document = "https://portal.uni-mannheim.de/qisserver/services/dbinterface?wsdl"
      wsdl.document = APP_CONFIG['soap_client']['lsf']['wsdl_url']
      #wsdl.endpoint= "https://ilias3.uni-stuttgart.de/webservice/soap/server.php?debug=1"
      #wsdl.namespace= "urn:ilUserAdministration"
    end
    (@wsdl_xml= Nokogiri::XML(@client.wsdl.xml)).remove_namespaces!
    @client.http.read_timeout = APP_CONFIG['soap_client']['lsf']['read_timeout']
    @notification= notification
  end

  # returns TempFile object to resource data fetched from LSF via SOAP call
  def fetch_data
    case @notification.type.name
    when "lecture" then get_lecture
    when "course_catalog" then get_course_catalog
    when "enrollments" then get_enrollments
    end
    Rails.logger.debug "LsfSoapClient#fetch_data: temp files: #{@notification.temp_files.map{|f| f.fpath.to_s}.join(', ')}"
  end

private

  # returns TempFile object to lecture data fetched from LSF via SOAP call 
  def get_lecture
    soap_call_xml = <<-EOF
    <SOAPDataService>
      <general>
        <object>#{APP_CONFIG['soap_client']['lsf']['db_interface']['lecture']}</object>
      </general>
      <condition>
        <LectureID>#{@notification.oid}</LectureID>
      </condition>
    </SOAPDataService>
    EOF
    response= make_lsf_soap_call(soap_call_xml)
    tf= TempFile.new(:ftype => "lsf_soap_data")
    tf.save_data(CGI.unescapeHTML(response.to_xml)).save!
    @notification.temp_files << tf
  end

  # returns TempFile object 
  def get_enrollments
    soap_call_xml = <<-EOF
    <SOAPDataService>
      <general>
        <object>#{APP_CONFIG['soap_client']['lsf']['db_interface']['enrollments']}</object>
      </general>
      <condition>
        <LectureID>#{@notification.oid}</LectureID>
        <GroupID>-1</GroupID>
      </condition>
    </SOAPDataService>
    EOF
    response= make_lsf_soap_call(soap_call_xml)
    tf= TempFile.new(:ftype => "lsf_soap_data")
    @notification.temp_files << tf
    tf.save_data(CGI.unescapeHTML(response.to_xml)).save!
  end

  # returns TempFile object to course catalog data fetched from LSF via SOAP call 
  def get_course_catalog
    soap_call_xml = <<-EOF
    <SOAPDataService>
      <general>
        <object>#{APP_CONFIG['soap_client']['lsf']['db_interface']['course_catalog']}</object>
      </general>
      <condition>
        <RootID>-1</RootID>
        <TermID>#{APP_CONFIG['soap_client']['lsf']['db_interface']['term_id']}</TermID>
      </condition>
    </SOAPDataService>
    EOF
    response= make_lsf_soap_call(soap_call_xml)
    tf= TempFile.new(:ftype => "lsf_soap_data")
    tf.save_data(CGI.unescapeHTML(response.to_xml)).save!
    @notification.temp_files << tf
  end
  

  def make_lsf_soap_call(soap_call_xml)
    servicename= @wsdl_xml.css('definitions > service').first.attr("name")
    lsf_proxy_soap_service_name= APP_CONFIG['soap_client']['queue']['servicename'] # workaround for simplews
    response= @client.request(((servicename== lsf_proxy_soap_service_name) ? :ins0 : :wsdl), "getDataXML") do |soap|
      soap.namespaces['xmlns:ins0'] = "urn:#{lsf_proxy_soap_service_name}" # workaround for simplews
      soap.body = {
        :arg0 => soap_call_xml,
        :attributes! => { :arg0 => { "xsi:type" => "xsd:string" } }
      }
    end
    response
  end


end

    #pp client.wsdl.soap_actions
    
    #    @@xml=[] 
    #    
    #    @@xml << <<EOF
    #<SOAPDataService>
    #  <general>
    #    <object>CCLecture</object>
    #  </general>
    #  <condition>
    #    <LectureID>121381</LectureID>
    #  </condition>
    #</SOAPDataService>
    #EOF
    #
    #@@xml << <<EOF
    #<SOAPDataService>
    #  <general>
    #    <object>CCEnrollments</object>
    #  </general>
    #  <condition>
    #    <LectureID>121381</LectureID>
    #    <GroupID>-1</GroupID>
    #  </condition>
    #</SOAPDataService>
    #EOF
    #
    #@@xml << <<EOF
    #<SOAPDataService>
    #  <general>
    #    <object>CCInstitutionsTopDown</object>
    #  </general>
    #  <condition>
    #    <RootID>2675</RootID>
    #    <TermID>20111</TermID>
    #  </condition>
    #</SOAPDataService>
    #EOF
    #
    #@@xml << <<EOF
    #<SOAPDataService>
    #  <general>
    #    <object>CCInstitutionsBottomUp</object>
    #  </general>
    #  <condition>
    #    <LeafID>2900</LeafID>
    #    <TermID>20111</TermID>
    #  </condition>
    #</SOAPDataService>
    #EOF
    
    #<RootID>-1</RootID>: points to root of tree
    #@@xml << <<EOF
    #<SOAPDataService>
    #  <general>
    #    <object>CCCourseCatalogTopDown</object>
    #  </general>
    #  <condition>
    #    <RootID>39813</RootID>
    #    <TermID>20121</TermID>
    #  </condition>
    #</SOAPDataService>
    #EOF
    
    #@@xml << <<EOF
    #<SOAPDataService>
    #  <general>
    #    <object>CCCourseCatalogBottomUp</object>
    #  </general>
    #  <condition>
    #    <LeafID>39319</LeafID>
    #    <TermID>20112</TermID>
    #  </condition>
    #</SOAPDataService>
    #EOF
    
    #@@xml << <<EOF
    #<SOAPDataService>
    #  <general>
    #    <object>
    #      CCLecture
    #    </object>
    #  </general>
    #  <condition>
    #    <LectureID>
    #      57519
    #    </LectureID>
    #  </condition>
    #</SOAPDataService>
    #EOF
    
    # takes place automatically
    #@@arg0.gsub!('<','\&lt;').gsub!('>','\&gt;')
    
     #@@xml.each do |rdata|
     #  response = client.request :wsdl, "getDataXML" do
     #    soap.body = { 
     #      :arg0 => rdata,
     #      :attributes! => { :arg0 => { "xsi:type" => "xsd:string" } }
     #    }
     #  end
     #  puts response.to_xml.gsub('&lt;','<').gsub('&gt;','>').gsub('&quot;','"')
     #end
