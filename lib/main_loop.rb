class MainLoop
  include Singleton
  require "lsfp_exceptions" 

  class << self
    attr_accessor :exception_tries
  end

  @exception_tries= 0

  def initialize                                                                                                              
    @queue= QueueSoapClient.new
    @notification= nil
    Rails.logger.info "*** LSF-Proxy started ***" 
  end                                                                                                                         

  def start
    loop do
      if (@notification= notification_recovery_object)
        # There is a notification right in the database, ready for further
        # processing.
        recoverymode= true
        type= @notification.type.name
        oid= @notification.oid
        pid= @notification.pid
        Rails.logger.info "-"*60
        Rails.logger.info \
          "MainLoop#start: recovered notification: " + \
          "type:#{type}, " + \
          "update_instance:#{@notification.update_instance}, " + \
          "object_id:#{oid}, protocol_id:#{pid}, " + \
          "status: #{@notification.status.name}"
      elsif (notification_str= @queue.fetch)
        # Fresh data from notification queue. Until now there is no
        # notification object representing this data. 
        type_value, oid, pid = notification_str.split(',').collect{|e| e.strip}
        Rails.logger.info "-"*60
        Rails.logger.info "MainLoop#start: new notification event from queue: type:#{type_value}, oid:#{oid}, pid:#{pid}"
        # set type to the key of type_value
        if (t= APP_CONFIG['notifications_types'].key(type_value))
          type= t
        else
          Rails.logger.error "MainLoop:#start: " + \
              "LSF sent notification event with unknown type value: #{type_value}, " + \
              "protocol_id: #{pid}, object_id: #{oid}. Throw it away."
          next # Make next loop iteration (skipping the rest).
        end
        # test if notification arrived with previous protocol_id
        if Notification.find_by_pid(pid)
          Rails.logger.error "MainLoop:#start: " + \
              "LSF sent notification event with protocol_id: #{pid} " + \
              "at least twice. Throw it away."
          next # Make next loop iteration (skipping the rest).
        end
        initial_instance= Notification.first_sent(type, oid)
        # TODO How could we determine if LSF deletes some objects?
        # creation of new notification
        @notification= Status.find_by_name("new").notifications.create!(
          :oid => oid.encode("UTF-8"),
          :pid => pid.encode("UTF-8"), 
          :update_instance => ((initial_instance)?true:false))
        Type.find_by_name(type).notifications << @notification
        recoverymode= false
        Rails.logger.info "MainLoop#start: create new notification: " + \
              "type:#{type}, " + \
              "update_instance:#{@notification.update_instance}, " + \
              "object_id:#{oid}, protocol_id:#{pid}"
      end

      if @notification
        # Statemachine
        case @notification.status.name
        when "new"
          # fetch LSF SOAP data
          LsfSoapClient.new(@notification).fetch_data
          Status.find_by_name("lsf_data_fetched").notifications << @notification
          @notification.temp_files.each do |f|
            Rails.logger.info \
              "MainLoop#start: " + \
              "fetched LSF data: type:#{@notification.type.name}, " + \
              "protocol_id:#{@notification.pid} " + \
              "file type:#{f.ftype} " + \
              "into file:#{File.basename(f.fpath)}"
          end
        when "lsf_data_fetched"
          # Process fetched LSF data
          ResourceProcessor.new(@notification).process_data
          @notification.temp_files.each {|f| f.remove_file}
          Status.find_by_name("processed").notifications << @notification
          @notification.representations.each do |representation|
            Rails.logger.info \
              "MainLoop#start: " + \
              "processed LSF data type:#{@notification.type.name}, " + \
              "protocol_id:#{@notification.pid} " + \
              "into representation file:#{representation.filename}"
          end
        when "processed"
          # notify ECS
          EcsResourceGenerator.new(@notification).send_resource
          Status.find_by_name("sent").notifications << @notification
          @notification.representations.each do |repr|
            Rails.logger.info \
              "MainLoop#start: " + \
              "send notification to ECS (#{repr.resource.name}/#{repr.rid})" + \
              "for mids:#{repr.receivers.map{|r| r.mid.to_s}.join(',')}"
          end
        end
      else
        sleep(3)
      end
      MainLoop.exception_tries= 0
    end
  rescue LsfProxy::NotificationFailureException => e
    Status.find_by_name("failure").notifications << @notification 
    Rails.logger.error "MainLoop#start:NotificationFailureException: Change status of notification (id=#{@notification.id}) to failure."
    Rails.logger.debug "Backtrace:\n"+e.backtrace.map{|l| " > "+l.to_s}.join("\n")
    retry if MainLoop.try_ones_more?
  rescue => e
    Rails.logger.error "MainLoop#start:Exception: #{e.class}: #{e.message}"
    Rails.logger.debug "Backtrace:\n"+e.backtrace.map{|l| " > "+l.to_s}.join("\n")
    retry if MainLoop.try_ones_more?
  end

  # only for testing
  def self.create_resource(type,oid,pid,xml_filepath)
    # test if notification arrived with previous protocol_id
    if Notification.find_by_pid(pid)
      Rails.logger.error "MainLoop:#start: " + \
          "LSF sent notification with protocol_id: #{pid} " + \
          "at least twice. Throw it away."
      return
    end
    initial_instance= Notification.first_sent(type, oid)
    # TODO How could we determine if LSF deletes some objects?
    # creation of new notification
    notification= Status.find_by_name("failure").notifications.create!(
      :oid => oid.encode("UTF-8"),
      :pid => pid.encode("UTF-8"), 
      :update_instance => ((initial_instance)?true:false))
    Type.find_by_name(type).notifications << notification
    recoverymode= false
    Rails.logger.info "new notification: " + \
          "type:#{type}, " + \
          "update_instance:#{notification.update_instance}, " + \
          "object_id:#{oid}, protocol_id:#{pid}"

    # Get LSF SOAP data file
    notification.filepath= xml_filepath
    notification.save!
    Status.find_by_name("lsf_data_fetched").notifications << notification
    Rails.logger.info \
      "fetched LSF data: type:#{type}, " + \
      "protocol_id:#{pid} into file: #{notification.filepath}"
  end

  def self.change_notification_status(notification_id, status)
    notification= Notification.find(notification_id)
    Status.find_by_name(status).notifications << notification
  end

private

  def notification_recovery_object
    # we are in recovery mode when there are valid notifications in the
    # database in combination with ecs_notifications_delayed configuration
    if LsfpHelper.ecs_notifications_delayed?
      (n= Notification.recovery_object_undelayed[0]) ? n : false
    else
      (n= Notification.recovery_object[0]) ? n : false
    end
  end

  def self.try_ones_more?
    if MainLoop.exception_tries < 5
      sleep(2**MainLoop.exception_tries)
      MainLoop.exception_tries+= 1
      true
    else
      false
    end
  end
  
end
