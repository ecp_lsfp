namespace :lsfp do
  desc "Stop LSF-Proxy."
  task :stop => :environment do
    lsfp_stop
  end

  desc "Start LSF-Proxy."
  task :start => :environment do
    lsfp_start
  end

  desc "Removes all notifications and the associated files."
  task :reset => :environment do
    num_of_representations_in_service= LsfpHelper.number_of_representations
    puts "*******"
    puts "Destroyed representations in service: #{LsfpHelper.number_of_representations}"
    puts "Destroyed delayed representations: #{LsfpHelper.number_of_delayed_representations}"
    puts "*******"
    Notification.destroy_all
  end

  desc "Enable notifications to ECS (DEPRICATED, use \"ecson\" instead)."
  task :enable_ecs_notifications => :environment do
    LsfpHelper.enable_ecs_notifications
  end

  desc "Enable notifications to ECS."
  task :ecson => :environment do
    LsfpHelper.enable_ecs_notifications
  end

  desc "Disable notifications to ECS (DEPRICATED, use \"ecsoff\" instead)."
  task :disable_ecs_notifications => :environment do
    LsfpHelper.disable_ecs_notifications
  end

  desc "Disable notifications to ECS."
  task :ecsoff => :environment do
    LsfpHelper.disable_ecs_notifications
  end

  desc "LSF-Proxy Status."
  task :status => :environment do
    puts "*******"
    if LsfpHelper.pid_file?
      pid= IO.read(LsfpHelper.pid_file).strip
      puts "LSF-Proxy is running (PID: #{pid})."
    else
      puts "LSF-Proxy is not running."
    end
    if LsfpHelper.ecs_notifications_delayed?
      puts "ECS notifications are disabled (delay on)."
    else
      puts "ECS notifications are enabled (delay off)."
    end
    puts "Number of representations in service: #{LsfpHelper.number_of_representations}"
    puts "Number of delayed representations: #{LsfpHelper.number_of_delayed_representations}"
    puts "Number of failed notifications: #{LsfpHelper.number_of_failed_notifications}"
    puts "*******"
  end

#  desc "Show the member_ships for LSF-Proxy."
#  task :show_memberships => :environment do
#  end
#
#
#  desc "This must be run after updating the LSF-Proxy software files."
#  task :update => :environment do
#  end

  def lsfp_start
    if LsfpHelper.pid_file?
      text="pid file exists (#{LsfpHelper.pid_file}).\nThere should be a running LSF-Proxy with process id: #{IO.read(LsfpHelper.pid_file).strip}."
      info text
      raise 'Aborting ...'
    end
    File.open(LsfpHelper.pid_file, 'w'){|f| f.puts Process.pid}
    begin
      MainLoop.instance.start
    ensure
      File.delete LsfpHelper.pid_file
    end
  end

  def lsfp_stop
    if LsfpHelper.pid_file?
      pid= IO.read(LsfpHelper.pid_file)
      Process.kill("TERM", pid.strip.to_i)
      info "LSF-Proxy terminated (PID:#{pid.strip})"
    else
      info "No PID file found (#{LsfpHelper.pid_file}).\nMaybe there is no LSF-Proxy running."
    end
  end

  def lsfp_running?
    LsfpHelper.pid_file?
  end
      
  def info text
    Rails.logger.info text
    puts "*******"
    puts "INFO: #{text}"
    puts "*******"
  end
end
