class HttpEcs

  require 'rest_client'
  include Singleton

  attr_reader :connection

  CONTENT_TYPE_URI_LIST = { 'Content-Type' => 'text/uri-list' }
  CONTENT_TYPE_JSON = { 'Content-Type' => 'application/json' }

  def initialize
    @connection= RestClient::Resource.new(
      APP_CONFIG['ecs']['url'],
      :ssl_client_cert  =>  OpenSSL::X509::Certificate.new(File.read(APP_CONFIG['ecs']['ssl_client_cert'])),
      :ssl_client_key   =>  OpenSSL::PKey::RSA.new(File.read(APP_CONFIG['ecs']['ssl_client_key']), APP_CONFIG['ecs']['ssl_client_key_pwd']),
      :ssl_ca_file      =>  APP_CONFIG['ecs']['ssl_ca_file'],
      :verify_ssl       =>  APP_CONFIG['ecs']['verify_ssl'],
      :headers          =>  { "Accept" => :json }
    )
    if APP_CONFIG['proxy'].blank?
      RestClient.proxy = nil
    else
      RestClient.proxy = APP_CONFIG['proxy']
    end
  end

end
