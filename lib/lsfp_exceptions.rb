module LsfProxy
  class NoReceiverMembershipsException < StandardError; end
  class NotificationFailureException < StandardError; end
end
