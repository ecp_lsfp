class EcsResourceGenerator

  require "lsfp_exceptions"

  def initialize(notification)
    @notification= notification
    @ecs= HttpEcs.instance
  end

  # create/update resource at ECS
  def send_resource
    @notification.representations.sort.each_with_index do |representation, idx|
      rmh= set_receiver_memberships_headers(representation, @ecs) 
      if rmh["X-EcsReceiverMemberships"].blank?
        txt= "EcsResourceGenerator#send_resource: notification (id=#{@notification.id}) has no receivers"
        raise LsfProxy::NoReceiverMembershipsException, txt
      end
      Rails.logger.debug "EcsResourceGenerator#send_resource#set_receiver_memberships_headers: #{rmh}"
      if location_ecs?(@notification)
        Rails.logger.debug "#{representation.resource.name} resource represenation data will be stored on ecs."
        @ecs.connection.headers.update HttpEcs::CONTENT_TYPE_JSON
        if @notification.update_instance
          representation.rid= rid(idx)
          representation.save!
          Rails.logger.debug "Update resource: @ecs.connection[#{representation.resource.name}/#{representation.rid}].put #{IO.read(json_tmp_path(representation.filename)).truncate(80)}"
          @ecs.connection["#{representation.resource.name}/#{representation.rid}"].put IO.read(json_tmp_path(representation.filename))
        else
          Rails.logger.debug "Create resource: @ecs.connection[#{representation.resource.name}].post IO.read(#{json_tmp_path(representation.filename)})"
          response= @ecs.connection[representation.resource.name].post IO.read(json_tmp_path(representation.filename))
          representation.rid= rid_from_location_header(response)
          representation.save!
        end
        File.delete(json_tmp_path(representation.filename))
      else
        Rails.logger.debug "#{representation.resource.name} resource represenation data will be stored on ecp."
        @ecs.connection.headers.update HttpEcs::CONTENT_TYPE_URI_LIST
        if @notification.update_instance
          current_filename= representation.filename
          previous_filename= @notification.previous.representations.sort[idx].filename
          FileUtils.copy(json_tmp_path(current_filename), json_resource_path(current_filename))
          representation.rid= rid(idx)
          representation.filename= previous_filename
          @ecs.connection["#{representation.resource.name}/#{representation.rid}"].put indirect_url(representation)
          FileUtils.mv(json_resource_path(current_filename), json_resource_path(previous_filename))
          representation.save!
          FileUtils.remove_file(json_tmp_path(current_filename))
        else
          FileUtils.copy(json_tmp_path(representation.filename), json_resource_path(representation.filename))
          response= @ecs.connection[representation.resource.name].post indirect_url(representation)
          representation.rid= rid_from_location_header(response)
          representation.save!
          FileUtils.remove_file(json_tmp_path(representation.filename))
        end
      end
    end
  rescue LsfProxy::NoReceiverMembershipsException => e
    Rails.logger.error e.to_s
    raise LsfProxy::NotificationFailureException
  rescue => e
    Rails.logger.error e.to_s
    raise
  end

private

  include LsfpHelper
  # set_receiver_memberships_headers(notification, ecs)

  def rid(idx)
    Notification.first_sent(@notification.type.name, @notification.oid).representations[idx].rid
  end

  def rid_from_location_header(response)
    rid= response.headers[:location].match(/\d+$/).to_s.to_i
    Rails.logger.debug "EcsResourceGenerator#rid_from_location_header: rid=#{rid} (notification id: #{@notification.id})"
    rid
  end

  def json_tmp_path(filename)
    File.join(ResourceProcessor::RESOURCE_TMP_DIR, filename)
  end

  def json_resource_path(filename)
    File.join(APP_CONFIG['ecp_resource_representations_dir'], filename)
  end

  def indirect_url(representation)
    iurl= File.join(APP_CONFIG['ecp_resource_representations_url'], representation.filename)
    if representation.notification.update_instance
      iurl= File.join(iurl, representation.rid.to_s)
    end
    iurl
  end

  def location_ecs?(notification)
    APP_CONFIG["resources"][notification.type.name]['location'] == 'ecs'
  end

end
