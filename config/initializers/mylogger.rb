logfilename= Rails.root.join("log",Rails.env + ".log")

class MyLogger < Logger
  def format_message(severity, time, progname, msg)
    "[%s(%d)%6s] %s\n" % [time.to_s(:db), $$, severity, msg.to_s]
  end
end
# which will rotate the log files every 5 megabytes and leave only the three
# most recent log files. This will limit the total spaces used by the logs at
# 15 megabytes.
Rails.logger= MyLogger.new(logfilename, 5, 1.megabytes)

Rails.logger.info "MyLogger: current log level: #{APP_CONFIG['debug_level']}"
case APP_CONFIG['debug_level']
  when "debug"
    Rails.logger.level= Logger::DEBUG
  when "info"
    Rails.logger.level= Logger::INFO
  when "error"
    Rails.logger.level= Logger::ERROR
  else
    Rails.logger.level= Logger::INFO
end
