class CreateAppConfigs < ActiveRecord::Migration
  def change
    create_table :app_configs do |t|
      t.boolean :delay_ecs_notifications

      t.timestamps
    end
  end
end
