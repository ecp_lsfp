class CreateRepresentations < ActiveRecord::Migration
  def change
    create_table :representations do |t|
      t.string :filename # obscured resource filename
      t.integer :rid # resource id at ECS
      t.integer :status_id
      t.integer :notification_id

      t.timestamps
    end
  end
end
