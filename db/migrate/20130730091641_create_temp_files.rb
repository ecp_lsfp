class CreateTempFiles < ActiveRecord::Migration
  def change
    create_table :temp_files do |t|
      t.string :fpath
      t.string :ftype
      t.integer :notification_id

      t.timestamps
    end
  end
end
