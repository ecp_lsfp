class CreateReceivers < ActiveRecord::Migration
  def change
    create_table :receivers do |t|
      t.string :name
      t.integer :mid

      t.timestamps
    end
  end
end
