class CreateReceiverRepresentations < ActiveRecord::Migration
  def change
    create_table :receiver_representations do |t|
      t.integer :receiver_id
      t.integer :representation_id

      t.timestamps
    end
  end
end
