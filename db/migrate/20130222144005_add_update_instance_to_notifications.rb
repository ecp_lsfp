class AddUpdateInstanceToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :update_instance, :boolean, :default => false
  end
end
