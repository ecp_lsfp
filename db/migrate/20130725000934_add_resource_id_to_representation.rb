class AddResourceIdToRepresentation < ActiveRecord::Migration
  def change
    add_column :representations, :resource_id, :integer
  end
end
