class RemoveFilepathFromNotification < ActiveRecord::Migration
  def up
    remove_column :notifications, :filepath
  end

  def down
    add_column :notifications, :filepath, :string
  end
end
