class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :oid
      t.string :pid
      t.string :filepath
      t.references :status, :default => 1
      t.references :type

      t.timestamps
    end
  end
end
