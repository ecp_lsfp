# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130730091641) do

  create_table "app_configs", :force => true do |t|
    t.boolean  "delay_ecs_notifications"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "notifications", :force => true do |t|
    t.string   "oid"
    t.string   "pid"
    t.integer  "status_id",       :default => 1
    t.integer  "type_id"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.boolean  "update_instance", :default => false
  end

  create_table "receiver_representations", :force => true do |t|
    t.integer  "receiver_id"
    t.integer  "representation_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "receivers", :force => true do |t|
    t.string   "name"
    t.integer  "mid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "representations", :force => true do |t|
    t.string   "filename"
    t.integer  "rid"
    t.integer  "status_id"
    t.integer  "notification_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "resource_id"
  end

  create_table "resources", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "statuses", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "temp_files", :force => true do |t|
    t.string   "fpath"
    t.string   "ftype"
    t.integer  "notification_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
